Quando("eu selecionar a opção {string} no menu") do |string|
  assert_no_selector("mat-spinner")
  assert_no_selector(".login-step")
  assert_no_selector(".overlay")
  sleep 4
  assert_no_selector(".block-preloader")
  assert_no_selector(".overlay")
  find("#mCSB_4_container").find(ELEM[string]).click
end

Quando("eu digitar a conta {string}") do |string|
  sleep 3
  find(ELEM["campo_conta_omni"]).set(MASSA[string])
end

Quando("eu clicar em {string}") do |string|
  sleep 1
  ##binding.pry
  find(:xpath, ELEM[string]).click
end

# Steps referentes ao Cenario @transf_conta_omni_agendu
# Clica no radio butom "Agendar transferência para:"
Quando("eu clicar no radio button {string}") do |string|
  find(ELEM[string]).click
end
# Clicar no botão para abrir a Agenda de Datas
Quando("eu clicar no botão Agenda de datas") do
  find(ELEM["Botao Calendario"]).click
end
# Selecionar a data de agendamento
Quando("eu selecionar uma data d + {string}") do |string|
  # Abre o calendário para escolha da data do agendamento
  data=find(".mat-calendar-period-button.mat-button").find(".mat-button-wrapper").text
  # Pega a data do dia em que estamos
  proxima_data=Date.parse(data)
  data_atual=Date.parse(data)
  # Acesso ao arquivo MASSA e pega o valor da chave "Dias a somar"
  proxima_data=proxima_data+(MASSA[string]).to_i
  # Subtrai 1 de proxima_data e Checa conteúdo do campo
  proxima_data=proxima_data-1
  # Checa se o dia agendado é Sábado
  proxima_data=proxima_data + 2 if proxima_data.saturday?
  # Checa se o dia agendado é Domingo
  proxima_data=proxima_data + 1 if proxima_data.sunday?
  # Acessa o calendário
  calendario=find(".mat-calendar")
  # Compara a data no calendário para encontrar o mês
  while proxima_data.month != data_atual.month
    # Enquanto não encontrar o mês da data agendada, clica no botão > do calendário
    find("button[aria-label='Next month']").click
    data_atual=data_atual.next_month
  end
  # Clica no dia do agendamento no calendário
  calendario.find(".mat-calendar-body-cell-content",text:proxima_data.day.to_s,exact_text:true).click
end

Quando("eu digitar o valor da transferência {string}") do |string|
  find(ELEM["campo_valor_transferencia"]).set(string)
  @valor_transf = find(ELEM["campo_valor_transferencia"]).value
end

Quando("eu verificar os {string} a ser tranferida") do |string|
  infos = find(ELEM[string])
  infos.assert_text(@valor_transf)
end

Quando("eu selecionar o banco {string}") do |string|
  sleep 3
  # VOLTAR ESTE BINDING PARA FALAR COM O VICTOR
  ##binding.pry
  find("input[formcontrolname='bank']").click
  find(ELEM[string]).click
  #find(ELEM[string]).click
end

Quando("eu verificar os dados da conta a ser tranferida - outros bancos - mesmo titular - ted") do
  assert_no_selector("mat-spinner")
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["conta"])
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["titularidade"])
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["Modalidade"])
  infos.assert_text(MASSA["Dados_CC_itau_ted"]["Tipo"])
  infos.assert_text(@valor_transf)
end

Quando("eu verificar os dados da conta a ser tranferida - outros bancos - mesmo titular - doc") do
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["conta"])
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["titularidade"])
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["Modalidade"])
  infos.assert_text(MASSA["Dados_CC_itau_doc"]["Tipo"])
  infos.assert_text(@valor_transf)
end

Quando("eu selecionar Outro Titular no campo Titularidade") do
  assert_no_selector("mat-spinner")
  sleep 3
  find("mat-select[formcontrolname='ownership']").click
  find("#mat-option-1").click
end

Entao("eu verificar os dados da conta a ser tranferida - outros bancos - outro titular") do
  assert_no_selector("mat-spinner")
  sleep 1
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu"]["conta"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu"]["titularidade"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu"]["Modalidade"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu']['nome outra conta doc'])
  #infos.assert_text(MASSA['Dados_CC_itau_doc']['Tipo'])
  infos.assert_text(@valor_transf)
end

Quando("eu selecionar a Modalidade {string}") do |string|
  sleep 3
  find("mat-select[formcontrolname='modality']").click
  sleep 0.5
  find(ELEM[string]).click
  ##binding.pry
end

Entao("eu verificar os dados da conta a ser tranferida - outros bancos - mesmo titular - doc - poupanca") do
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["conta"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu_poup']['titularidade'])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["Modalidade"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu']['nome outra conta doc'])
  #infos.assert_text(MASSA['Dados_CC_itau_doc']['Tipo'])
  infos.assert_text(@valor_transf)
end

Entao("eu verificar os dados da conta a ser tranferida - doc - outros bancos - outro titular - poupanca") do
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["conta"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["titularidade"])
  infos.assert_text(MASSA["Dados_CC_itau_doc_outra_titu_poup"]["Modalidade"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu']['nome outra conta doc'])

  #infos.assert_text(MASSA['Dados_CC_itau_doc']['Tipo'])
  infos.assert_text(@valor_transf)
end

Entao("eu verificar os dados da conta a ser tranferida - outros bancos - mesmo titular - ted - poupanca") do
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["conta"])
  #infos.assert_text(MASSA['Dados_CC_itau_ted_outra_titu_poup']['titularidade'])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["Modalidade"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu']['nome outra conta doc'])

  #infos.assert_text(MASSA['Dados_CC_itau_doc']['Tipo'])
  infos.assert_text(@valor_transf)
end

Entao("eu verificar os dados da conta a ser tranferida - ted - outros bancos - outro titular - poupanca") do
  infos = find(".list-confirmation")
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["Banco"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["agencia"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["conta"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["titularidade"])
  infos.assert_text(MASSA["Dados_CC_itau_ted_outra_titu_poup"]["Modalidade"])
  #infos.assert_text(MASSA['Dados_CC_itau_doc_outra_titu']['nome outra conta doc'])

  #infos.assert_text(MASSA['Dados_CC_itau_doc']['Tipo'])
  infos.assert_text(@valor_transf)
end

Quando("eu digitar a assinatura eletronica com {string}") do |string|
  # #binding.pry
  find(ELEM["Assinatura eletronica"]).click
  assert_selector ".keyboard-holder"
  digitos = MASSA[string].split("")
  digitos.each do |digito|
    find(".keyboard-button", text: digito).click
  end
end

# Mensagem de Aviso de Operação fora do horário permitido
Quando("eu verificar a {string}") do |string|
  ##binding.pry
  msg=find(".modal.container")
  ##binding.pry
  msg.assert_text(MASSA["Mensagem de Aviso do Horario"])
end

# Mensagem de Assinatura Eletronica Divergente
Então("eu verifico a {string}") do |string|
  assert_text(MASSA["Mensagem de Assinatura Divergente"], wait: 45)
end


Entao("eu recebo a mensagem de sucesso") do
  assert_no_text("Não foram encontradas opções de investimento para o período e valor informados. ")
  assert_no_selector("app-warning[ng-reflect-type='error']")
  assert_selector(".ico-success")
  sleep 3
  find(".btn-logout.non-printable")
  sleep 2
end

# Mensagem de Erro de Valor
Então("eu recebo a mensagem de erro") do
  ##binding.pry
  assert_selector(".error")
  ##binding.pry
end