Dado("que eu esteja no ambiente {string}") do |string|
  visit(MASSA[string])
  sleep 10
  #assert_selector("div[data-component='login']", wait: 30)
end

Quando("eu preencher o campo {string} com {string}") do |string, string2|
  # #binding.pry
  find(ELEM[string]).set(MASSA[string2])
  sleep 0.4
end

# 15/05/19 - VALMIR
# Trocamos a chamada pois foi incluido mais um botão Continuar na tela e isso
# causou problema na automação por duplicidade de elementos na tela.
# Este passo está sendo utilizado para chamar alguns botões Continuar e Entrar
Quando("eu clicar no botao {string}") do |string|
  #click_button(string, wait: 10)
  find(ELEM[string],text:string).click
end

Quando("eu digitar a senha {string}") do |string|
  assert_selector("input[formcontrolname='pass']")
  digitos = MASSA[string].split("")
  digitos.each do |digito|
    find(".keyboard-button", text: digito).click
  end
end

Entao("eu espero visualizar a home do internet banking") do
  assert_no_selector("mat-spinner")
  #assert_text("Olá, Automação.")
  assert_text("Minha conta", wait:15)
  assert_selector("div[data-component='profile']")
end

Entao("eu espero visualizar a mensagem de erro {string}") do |string|
  assert_selector("snack-bar-container", text: string)
end

Entao("eu espero visualizar aviso de erro {string}") do |string|
  assert_text(string)
end

Entao("eu espero visualizar mensagem de aviso abaixo do campo senha") do
  find(".form-control", text: "Senha").assert_selector(".error")
end

Entao("eu espero que o botao {string} não seja habilitado") do |string|
  #binding.pry
end

Entao("eu espero visualizar a mensagem {string}") do |string|
  assert_text string
end

Quando("eu clicar no link {string}") do |string|
  find(ELEM[string]).click
end

Entao("eu espero que seja aberta a tabela de tarifas") do
  driver.title
end