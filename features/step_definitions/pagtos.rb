Quando("eu processar as infos do boleto") do
  sleep 1
  assert_no_selector("mat-spinner")
  raise find("simple-snack-bar").text if all("simple-snack-bar").size > 0
  @nr = find("input[formcontrolname='barCode']").value.gsub(".", "")
  @valor = find("input[formcontrolname='value']").value
  find("input[formcontrolname='value']").click
  find("input[formcontrolname='dueDateInput']").value.nil?
end

Entao("eu valido as informações do boleto") do
  sleep 1
  #binding.pry
  assert_no_selector("mat-spinner")
  assert_text(@nr)
  assert_text(@valor)
  #binding.pry
end

# Pagamento de Boleto Vencido
Entao("eu recebo mensagem de que o boleto esta vencido") do
  find("div[data-component='warning'][class='error']")
  assert_text("Data de movimento maior que a data limite para pagamento do boleto.")
end