Entao("eu espero visualizar a tela de comprovantes") do
    assert_selector("div[data-component='receipts']")
end
 
  Quando("eu seleciono o periodo {string}") do |string|
    filtro = find("div[data-component='special-filter']")
    filtro.find(".btn-filter", text: string).click
  end

  Quando("eu seleciono o periodo inicial d - {int}") do |int|
    datepick = (".period-container.box")
    find_all(".datepicker-calendar.material-icons")[0].click
    data = find(".mat-calendar-period-button.mat-button").text
    proxima_data=Date.parse(data)
    data_atual=Date.parse(data)
    # Acesso ao arquivo MASSA e pega o valor da chave "Dias a somar"
    proxima_data=proxima_data - int
    # Acessa o calendário
    calendario=find(".mat-calendar")
    # Compara a data no calendário para encontrar o mês
    while proxima_data.month != data_atual.month
      # Enquanto não encontrar o mês da data agendada, clica no botão > do calendário
      find("button[aria-label='Previous month']").click
      data_atual=data_atual.prev_month
    end
    calendario.find(".mat-calendar-body-cell-content",text:proxima_data.day.to_s,exact_text:true).click
  end
  
  Quando("eu seleciono o periodo final d - {int}") do |int|
    datepick = (".period-container.box")
    find_all(".datepicker-calendar.material-icons")[1].click
    data = find(".mat-calendar-period-button.mat-button").text
    proxima_data=Date.parse(data)
    data_atual=Date.parse(data)
    # Acesso ao arquivo MASSA e pega o valor da chave "Dias a somar"
    proxima_data=proxima_data - int
    # Acessa o calendário
    calendario=find(".mat-calendar")
    # Compara a data no calendário para encontrar o mês
    while proxima_data.month != data_atual.month
      # Enquanto não encontrar o mês da data agendada, clica no botão > do calendário
      find("button[aria-label='Previous month']").click
      data_atual=data_atual.prev_month
    end
    calendario.find(".mat-calendar-body-cell-content",text:proxima_data.day.to_s,exact_text:true).click
  end
  Entao("eu espero visualizar os links {string}") do |string|
    assert_text(string)
  end