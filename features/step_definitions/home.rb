Quando("eu logar no sistema") do
  find(ELEM['cpf']).set(MASSA['cpf valido'])
    click_button('Próximo')
    assert_selector("input[formcontrolname='pass']", wait: 30)
    sleep 3
    digitos = MASSA['senha valida'].split("")
      digitos.each do |digito|
        find(".keyboard-button", text: digito).click
        sleep 0.3
      end
      click_button('Entrar', wait: 30)
      
  end
  
  Quando("eu selecionar a conta {string}") do |string|
    find("mat-list-item", text:MASSA[string]).click
    end
  
  Entao("eu espero visualizar a tela principal da conta") do
    ##binding.pry
    assert_selector("div[data-component='recent-releases']")
  end
  
  Entao("eu espero visualizar a area de {string}") do |string|
    assert_no_selector("mat-spinner")
    assert_no_text("Carregando...",wait:15)
    assert_selector(ELEM[string],wait:15)
  end
  
  Entao("eu espero visualizar a opção {string}") do |string|
    assert_selector(ELEM[string])
  end

  Quando("eu selecionar o botao {string}") do |string|
    sleep 1
    assert_no_selector("mat-spinner")
    find(".btn.border", text: string).click
  end

  Quando("eu clicar no filtro {string}") do |string|
    sleep 1
    assert_no_selector("mat-spinner")
    find(".btn-filter", text: string).click
  end
  
  Entao("eu espero visualizar a tela Extrato de conta") do
    assert_selector("mat-table")
  end
  

    # Visualizar campos referentes a conta corrente PJ na tela inicial
  Entao("eu espero visualizar {string}") do |string|
    #binding.pry
    assert_selector(ELEM[string])
  end

# Este código verifica o conteudo que foi carregado para os camps referentes
# aos dados da conta corrente PJ na tela inicial
  Entao("eu espero visualizar {string} com o dado carregado") do |string|
    #binding.pry
    infos = find(ELEM[string])
    #binding.pry
    infos.assert_text(MASSA[string]["nome do cliente"])
    infos.assert_text(MASSA[string]["numero do CNPJ"])
    infos.assert_text(MASSA[string]["numero agencia e conta"])
  end

  Entao("eu espero visualizar o campo {string}") do |string|
    assert_selector(ELEM[string],text:string)
  end