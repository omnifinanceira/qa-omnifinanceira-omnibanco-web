Quando("eu preencher o campo do valor do investimento com {string}") do |string|
    find("#invest-value").set(string)
    @valor_transf = find("#invest-value").value
  end
  
  Quando("eu selecionar o periodo de {string}") do |string|
    assert_selector(".time-list")
    find(".time-list-item", exact_text: string).click
  end
  
  Quando("eu selecionar a ultima opção") do
    sleep 1
    assert_no_selector("mat-spinner")
    op = all(".block.box.invest-option")
    assert_no_selector(".overlay")
    @nome_inv = op.last.find(".header").text
    assert_no_selector(".overlay")
    op.last.click_button("Investir")    
  end

  Entao("eu espero que os dados estejam corretos para o investimento") do
   assert_text(@nome_inv)
   assert_text(@valor_transf)
  end
  
  Entao("eu valido que o investimento foi realizado com sucesso") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando("eu selecionar a opção Extrato no menu") do
    find("a[href='/investimentos/extrato']").click
  end
  
  Quando("eu visualizar a lista de investimentos") do
    sleep 2
    assert_no_selector("mat-spinner")
    assert_selector(".overlay")
    assert_no_selector(".overlay")
    @opcs = all(".box")
    raise "não há investimentos a resgatar" if @opcs.size < 1
  end
  
  Quando("eu selecionar uma aplicação") do
    sleep 2
    assert_no_selector("mat-spinner")
    sleep 1
    @nome = @opcs[1].find(".title.col-md-6").text
    @nr = @opcs[1].find(".number.col-md-6").text
    @vlr = @opcs[1].find(".progress-bar-description").text
    @opcs[1].click_button("Visualizar")
  end
  
  Quando("eu visualizar os dados da aplicação") do
   assert_text(@nome)
   assert_text(@nr)
   assert_text(@vlr)
  end
  
  Quando("eu resgatar a aplicação") do
        click_button("Realizar resgate")
  end
  
  Quando("eu selecionar o modelo de resgate da aplicação como {string}") do |string|
    pending # Write code here that turns the phrase above into concrete actions
  end
