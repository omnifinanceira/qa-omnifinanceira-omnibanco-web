require "selenium-webdriver"
require "base64"
ENV["http_proxy"] = nil

Before do
  @suporte = Suporte.new
  pagina = page.driver.browser.manage.window.maximize
  pagina.maximize
end

AfterStep do
  screenshot = save_screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
  a = Base64.encode64(File.open(screenshot, "rb").read)
  embed(a, "image/png:base64", "Evidencia - Telas")
end

After do |scenario|
  ##binding.pry
  #binding.pry if scenario.failed? == true
  screenshot = save_screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
  a = Base64.encode64(File.open(screenshot, "rb").read)
  embed(a, "image/png:base64", "Evidencia - FIM ")
  Capybara.current_session.driver.browser.manage.delete_all_cookies
  #  page.execute_script('sessionStorage.clear()')
  Capybara.current_session.driver.quit
end
