#language:pt
@extratos_da_conta
Funcionalidade: Validar a funcionalidade de extratos da conta

@acessar_tela_extrato
Cenario: Acessar a tela de comprovantes
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Extrato de conta" no menu
Entao eu espero visualizar a tela Extrato de conta

@busca_extrato_hoje 
Cenario: Realizar consulta de extrato de hoje
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar o botao "Ver extrato completo"
E eu clicar no filtro "Hoje"
Entao eu espero visualizar a tela Extrato de conta

@busca_extrato_15_dias
Cenario: Realizar consulta de extrato de 15 dias
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar o botao "Ver extrato completo"
E eu clicar no filtro "15 dias"
Entao eu espero visualizar a tela Extrato de conta

@busca_extrato_este_mes
Cenario: Realizar consulta de extrato do mês
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar o botao "Ver extrato completo"
E eu clicar no filtro "30 dias"
Entao eu espero visualizar a tela Extrato de conta

@busca_extrato_por_periodo
Cenario: Realizar consulta de extrato por período
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar o botao "Ver extrato completo"
E eu clicar no filtro "Período"
E eu seleciono o periodo inicial d - 30
E eu seleciono o periodo final d - 15
E eu clicar no botao "Filtrar"
Entao eu espero visualizar a area de "Extratos da conta"