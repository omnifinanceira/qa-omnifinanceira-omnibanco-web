#language:pt
@minha_conta
Funcionalidade: Minha Conta

Cenario: Validar funcionalidades tela Minha Conta
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Minha Conta" no menu
Entao eu espero visualizar os links "Meus Limites"
E eu espero visualizar os links "Renomear Conta"
E eu espero visualizar os links "Trocar senha de acesso"
E eu espero visualizar os links "Trocar assinatura eletrônica"
E eu espero visualizar os links "Gerenciar usuários"

@meus_limites
Cenario: Validar tela Meus Limites
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Minha Conta" no menu
E eu selecionar a opção "Meus Limites" no menu 
Entao eu espero visualizar o campo "Meus limites"
E eu espero visualizar o campo "Transferências"
E eu espero visualizar o campo "Pagamentos"
E eu espero visualizar o campo "Tributos e Convênios"
E eu espero visualizar o campo "Investimentos"



