#language:pt
@login
Funcionalidade: Login no aplicativo

Cenario: Quando eu realizo o login com sucesso o sistema apresenta as contas cadastradas no cpf
Dado que eu esteja no ambiente "pre_prod"
Quando eu preencher o campo "cpf" com "cpf valido"
E eu clicar no botao "Próximo"
E eu digitar a senha "senha valida"
E eu clicar no botao "Entrar"
E eu selecionar a conta "PF"
E eu clicar no botao "Entrar"
Entao eu espero visualizar a home do internet banking