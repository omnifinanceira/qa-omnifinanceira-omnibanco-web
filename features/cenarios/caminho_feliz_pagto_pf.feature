#language:pt
@pagamento 
Funcionalidade: Validar caminho feliz pagamento de contas

Contexto:
Dado que eu esteja no ambiente "pre_prod"
E eu logar no sistema
E eu selecionar a conta "PF"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Pagamentos" no menu

# 20190520_006 EXECUTAR TESTE DE PAGAMENTO DE BOLETO A VENCER
# Código Alterado por: VALMIR SOARES
# Código de Barras do Boleto Utilizado: "34190310049883377000500000000000878960007316246uc"
# Vencimento: 21/05/2019
# Pagamento Efetuado em 20/05/2019 - Após o horário permitido 18HS
# Testes Efetuado em: 20/05/2019
# Teste Rodado por: VALMIR SOARES 
@tipo_1
Cenario: Realizar um Pagamento de boleto 1
Quando eu preencher o campo "Código de barras" com "Boleto tipo 1"
E eu processar as infos do boleto
E eu clicar no botao "Continuar"
E eu valido as informações do boleto
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190517_002 EXECUTAR TESTE DE AGENDAMENTO DE PAGAMENTO DE BOLETO A VENCER
# Código Alterado por: VALMIR SOARES
# Código de Barras do Boleto Utilizado: "34190240170477057000200000000000478980005288261"
# Vencimento: 20/05/2019
# Dias a Somar: 10
# Pagamento Agendado para dia: 04/06/2019
# Teste efetuado em 17/05/2019 - COM SUCESSO
@tipo_1_pagato_agendado
Cenario: Realizar o Agendamento de um Pagamento de Boleto a Vencer
Quando eu preencher o campo "Código de barras" com "Boleto tipo 1"
E eu processar as infos do boleto
#E eu clicar em "Pagar hoje"
# Agendamento de Pagamento
E eu clicar no radio button "Agendar pagamento para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu valido as informações do boleto
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

@tipo_2
Cenario: Realizar um Pagamento de boleto 2
Quando eu preencher o campo "Código de barras" com "Boleto tipo 2"
E eu processar as infos do boleto
#E eu clicar em "Pagar hoje"
E eu clicar no botao "Continuar"
Entao eu valido as informações do boleto

@tipo_3
Cenario: Realizar um Pagamento de boleto 3
Quando eu preencher o campo "Código de barras" com "Boleto tipo 3"
E eu processar as infos do boleto
#E eu clicar em "Pagar hoje"
E eu clicar no botao "Continuar"
Entao eu valido as informações do boleto

@tipo_4
Cenario: Realizar um Pagamento de boleto 4
Quando eu preencher o campo "Código de barras" com "Boleto tipo 4"
E eu processar as infos do boleto
#E eu clicar em "Pagar hoje"
E eu clicar no botao "Continuar"
Entao eu valido as informações do boleto

# 20190517_003 EXECUTAR TESTE DE PAGAMENTO DE BOLETO VENCIDO
# Código Alterado por: VALMIR SOARES
# Código de Barras do Boleto Utilizado: "34190360111485301000800000000000478290015687274"
# Vencimento: 15/03/2019
# Pagamento Efetuado em 17/05/2019 
# Teste efetuado em 17/05/2019 - 
@tipo_1_boleto_vencido
Cenario: Realizar o Pagamento de um boleto vencido
Quando eu preencher o campo "Código de barras" com "Boleto vencido tipo 1"
E eu processar as infos do boleto
E eu clicar no botao "Continuar"
E eu valido as informações do boleto
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Entao eu recebo mensagem de que o boleto esta vencido
