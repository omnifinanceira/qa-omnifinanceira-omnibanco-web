#language:pt
@comprovante
Funcionalidade: Validar a funcionalidade de comprovantes

@acessar_tela_comprovantes
Cenario: Acessar a tela de comprovantes
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Comprovantes" no menu
Entao eu espero visualizar a tela de comprovantes 

@filtra_extrato_15_dias
Cenario: Realizar consulta de comprovante de 15 dias
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Comprovantes" no menu
E eu seleciono o periodo "15 dias"
Entao eu espero visualizar a area de "operações do periodo"

@filtra_extrato_30_dias
Cenario: Realizar consulta de comprovante do Mês
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Comprovantes" no menu
E eu seleciono o periodo "30 dias"
Entao eu espero visualizar a area de "operações do periodo"

@comprovante_periodo
Cenario: Realizar consulta de comprovante por Periodo
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Comprovantes" no menu
E eu seleciono o periodo "Período"
E eu seleciono o periodo inicial d - 30
E eu seleciono o periodo final d - 15
E eu clicar no botao "Filtrar"
Entao eu espero visualizar a area de "operações do periodo"
