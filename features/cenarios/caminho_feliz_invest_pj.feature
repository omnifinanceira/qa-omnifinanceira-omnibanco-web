#language:pt
@investimentos_
Funcionalidade: Validar caminho feliz de Investimento - Pessoa juridica

Contexto:
Dado que eu esteja no ambiente "pre_prod"
E eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Investimentos" no menu

@tela_extrato
Cenario: Validar campos na tela de Extrato de Investimentos
Quando eu selecionar a opção "Extrato" no menu
Então eu espero visualizar o campo "Investido"
E eu espero visualizar o campo "Rendimento"
E eu espero visualizar o campo "Resgate Bruto"
E eu espero visualizar o campo "Impostos"
E eu espero visualizar o campo "Resgate Líquido"
E eu espero visualizar a area de "Distribuição das aplicações"
E eu espero visualizar a area de "Histórico de movimentação"

# 201905_001-EXECUTA UM IINVESTIMENTO DE 30 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_30_d
Cenario: Realizar um investimento de 30 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "30 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_002-EXECUTA UM IINVESTIMENTO DE 60 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_60_d
Cenario: Realizar um investimento de 60 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "60 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_003-EXECUTA UM IINVESTIMENTO DE 90 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_90_d
Cenario: Realizar um investimento de 90 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "90 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_004-EXECUTA UM IINVESTIMENTO DE 120 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_120_d
Cenario: Realizar um investimento de 120 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "120 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_005-EXECUTA UM IINVESTIMENTO DE 150 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_150_d
Cenario: Realizar um investimento de 150 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "150 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_006-EXECUTA UM IINVESTIMENTO DE 180 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_180_d
Cenario: Realizar um investimento de 180 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "180 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_007-EXECUTA UM IINVESTIMENTO DE 360 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_360_d
Cenario: Realizar um investimento de 360 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "360 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso


# 201905_008-EXECUTA UM IINVESTIMENTO DE 720 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_720_d
Cenario: Realizar um investimento de 720 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "720 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_009-EXECUTA UM IINVESTIMENTO DE 1080 DIAS
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@invest_1080_d
Cenario: Realizar um investimento de 1080 dias com sucesso
Quando eu selecionar a opção "Simulação" no menu
E eu preencher o campo do valor do investimento com "0150000"
E eu selecionar o periodo de "1080 dias"
E eu clicar no botao "Continuar"
E eu selecionar a ultima opção
E eu espero que os dados estejam corretos para o investimento
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_010-EXECUTA UM RESGATE DE INVESTIMENTO COM VALOR TOTAL
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@resgate_total
Cenario: Realizar um resgate de investimento
Quando eu selecionar a opção "Extrato" no menu
E eu visualizar a lista de investimentos
E eu selecionar uma aplicação
E eu visualizar os dados da aplicação
E eu resgatar a aplicação
E eu clicar em "Total"
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_011-EXECUTA UM RESGATE DE INVESTIMENTO COM VALOR PARCIAL
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
@resgate_parcial
Cenario: Realizar um resgate parcial de um investimento.
Quando eu selecionar a opção "Extrato" no menu
E eu visualizar a lista de investimentos
E eu selecionar uma aplicação
E eu visualizar os dados da aplicação
E eu resgatar a aplicação
E eu clicar em "Parcial" 
E eu preencher o campo "Valor" com "Valor do titulo"
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso