#language:pt
@home
Funcionalidade: validar a funcionalidade dos itens da tela inicial do aplicativo

@valida_conta_pj
Cenario: ao logar eu espero visualizar as areas de Ultimos lançamentos, Evolução da conta, O que deseja a seguir, para conta PJ
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
Entao eu espero visualizar a tela principal da conta
E eu espero visualizar a area de "Lançamentos mais recentes"
E eu espero visualizar a area de "Menu lateral"
E eu espero visualizar a opção "Sair"

@valida_conta_pf @pf
Cenario: Ao logar eu espero visualizar as areas de Ultimos lançamentos, Evolução da conta, O que deseja a seguir, para conta PF
Dado que eu esteja no ambiente "pre_prod"
Quando eu logar no sistema
E eu selecionar a conta "PF"
E eu clicar no botao "Entrar"
Entao eu espero visualizar a tela principal da conta
E eu espero visualizar a area de "Lançamentos mais recentes"
E eu espero visualizar a area de "Menu lateral"
E eu espero visualizar a opção "Sair"



