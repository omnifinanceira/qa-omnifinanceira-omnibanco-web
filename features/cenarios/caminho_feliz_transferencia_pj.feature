# TESTE: VALIDAÇÃO DO CAMINHO FELIZ PARA TRANSFERÊNCIAS EFETUADAS NO SISTEMA
#language:pt
@transferencia_pj @transferencias
Funcionalidade: Validar caminho feliz da transferência

Contexto:
Dado que eu esteja no ambiente "pre_prod"
E eu logar no sistema
E eu selecionar a conta "PJ"
E eu clicar no botao "Entrar"
E eu selecionar a opção "Transferências" no menu

# 201905_001-EXECUTA UMA TRANSFERÊNCIA ENTRE AS CONTAS DA OMNI
# Código Implementado por: VICTOR DEBAZA
# Ultima Data de Execução de Teste: 16/05/2019
# Neste cenário o sistema deve executar uma transferência entre contas da Omni
@transf_conta_omni
Cenario: Realizar uma transferencia para outra contra omni
Quando eu selecionar a opção "Entre contas Omni Banco" no menu
#Quando eu selecionar a opção "Entre conta Omni" no menu
E eu digitar a conta "conta omni"
E eu digitar o valor da transferência "10000"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta omni" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_002-EXECUTA AGENDAMENTO DE TRANSFERÊNCIA ENTRE CONTAS OMNI
# Código Implemtnado por: VALMIR SOARES
# Última Data de Execução de Teste: 16/05/2019
# Neste cenário o sistema deve executar o agendamento de uma Transferência para Conta Corrente
# entre contas da Omni
@transf_conta_omni_agendu 
Cenario: Realizar o agendamento de uma transferencia entre contas da Omni
Quando eu selecionar a opção "Entre contas Omni Banco" no menu
E eu digitar a conta "conta omni"
E eu digitar o valor da transferência "165550"
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta omni" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190626_003-EXECUTA UMA TRANSFERÊNCIA TED PARA OUTRO BANCO E MESMA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 26/06/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve exectuar uma TED para Conta Corrente,
# de Outro Banco e Mesma titularidade
# Teste para verificação do comprovante do TED ao final do mesmo
@transf_outro_banco_ted 
Cenario: Realizar um TED para outro banco 
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "100000" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_004-EXECUTA AGENDAMENTO DE TRANSFERÊNCIA DO TIPO TED PARA OUTRO BANCO, PARA MESMA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 16/05/2019
# Neste cenário testamos o agendamento de Transferência via TED para Conta Corrente, 
# de Outro banco e para a Mesma Titularidade
@transf_outro_banco_ted_agendu
Cenario: Realizar o agendamento de uma TED para outro banco 
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "123456"
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190529_001-EXECUTA UMA TRANSFERÊNCIA DOC PARA OUTRO BANCO E MESMA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 29/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar um DOC para Conta Corrente
# de Outro Banco e Mesma titularidade, dentro do horário máximo parametrizado no sistema 20HS
# Este procedimento vai testar um doc de valor acima do máximo permitido 4.999,99
@transf_outro_banco_doc 
Cenario: Realizar um DOC para outro banco
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "355550" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190522_012-EXECUTA AGENDAMENTO DE TRANSFERÊNCIA DO TIPO DOC PARA OUTRO BANCO, PARA MESMA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 22/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar o agendamento de Transferência via DOC, para conta corrente 
# de Outro banco e Mesma Titularidade.
# Agendamento ocorreu para um Dia Não Útil - Feriado Nacional (20/06/2019)
@transf_outro_banco_doc_agendu 
Cenario: Realizar o agendamento de um DOC para outro banco
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "200612" 
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190522_008-EXECUTA UMA TRANSFERÊNCIA DOC PARA OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 22/05/2019
# Teste Rodado Por: VALMIR SOARES
# Neste cenário o sistema deve executar um DOC para Conta Corrente
# de outro banco e outra titularidade - Antes do Horário Máximo das 17:00HS
@transf_outro_banco_outro_titular_doc 
Cenario: Realizar um DOC para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta doc"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "220508" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190523_001-EXECUTA AGENDAMENTO DE TRANSFERÊNCIA DO TIPO DOC PARA OUTRO BANCO, E OUTRA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 23/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar o agendamento de Transferência via DOC, 
# para Conta Corrente de Outro banco e outra Titularidade
# Agendamento ocorreu para um Dia Não Útil - Feriado Nacional (20/06/2019)
@transf_outro_banco_outro_titular_doc_agendu
Cenario: Realizar o agendamento de um DOC para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta doc"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "230501"
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190626_001-EXECUTA UMA TRANSFERÊNCIA TED PARA OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 26/06/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário vamos validar as informações no recibo de efetivação da transação de TED 
@transf_outro_banco_outro_titular_ted 
Cenario: Realizar um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "260619" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

###########################################################################################
########### TRANSFERENCIA TED CARACTERES ESPECIAIS CAMPO VALOR - MENSAGEM DE ERRO #########
###########################################################################################
# 20190527_003-EXECUTA UMA TRANSFERÊNCIA TED PARA OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 27/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar uma TED para Conta Corrente de outro banco e de Outra Titularidade
# Testar a utilização de valor Zero no campo de Valor - Deve exibir Mensagem de Erro
@transf_outro_banco_outro_titular_ted_erro_valor 
Cenario: Realizar um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "0" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de erro

###########################################################################################
########### TRANSFERENCIA TED VALOR ACIMA DO SALDO EXISTENTE - MENSAGEM DE ERRO ###########
###########################################################################################
# 20190626_003-EXECUTA UMA TRANSFERÊNCIA TED PARA OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 26/06/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar uma TED para Conta Corrente de outro banco e de Outra Titularidade
# utilizando um valor acima do saldo existente na conta corrente do cliente - Deve exibir Mensagem de Erro

# @transf_outro_banco_outro_titular_ted_valor_acima_do_saldo
# Cenario: Realizar um TED para outro banco e outro titular
# Quando eu selecionar a opção "Outros bancos" no menu
# E eu clicar em "TED"
# E eu clicar no botao "Continuar"
# E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
# E eu preencher o campo "Agencia" com "agencia itau"
# E eu preencher o campo "Conta" com "conta itau"
# E eu selecionar Outro Titular no campo Titularidade
# E eu preencher o campo "Nome" com "nome outra conta ted"
# E eu preencher o campo "CPF" com "cpf outra conta"
# E eu clicar no botao "Continuar"
# E eu digitar o valor da transferência "80050" 
# E eu clicar no botao "Continuar"
# E eu verificar os "dados da conta outro banco" a ser tranferida
# E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
# E eu preencher o campo "Código do seu Token" com "Token"
# E eu clicar no botao "Continuar"
# Então eu recebo a mensagem de erro

# 20190523_012-EXECUTA AGENDAMENTO DE TRANSFERÊNCIA DO TIPO TED PARA OUTRO BANCO, E OUTRA TITULARIDADE
# Código Implemtnado por: VALMIR SOARES
# Última Data de Execução de Teste: 23/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar o agendamento de uma Transferência via TED, 
# para Conta Corrente de Outro banco e para Outra Titularidade
# Dias a Somar: 4 ou seja será para dia 26/05/2019 - Dia Náo Útil - Domingo
# Sistema deve agendar para dia: 27/05/2019 - Próximo Dia Útil
@transf_outro_banco_outro_titular_ted_agendu
Cenario: Realizar o agendamento de uma TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "230512"
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

###########################################################################################
############### TRANSFERENCIA APÓS O HORÁRIO PERMITIDO - MENSAGEM DE AVISO! ###############
###########################################################################################
# 20190531_001-EXECUTA TRANSFERÊNCIA DO TIPO TED PARA OUTRO BANCO, E OUTRA TITULARIDADE
# Código Implemtnado por: VALMIR SOARES
# Última Data de Execução de Teste: 31/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar uma Transferência via TED, 
# para Conta Corrente de Outro banco e para Outra Titularidade,
# Após o Horário Máximo permitido, que é 20:00HS e deve ser exibida a Mensagem que Avisa
# esta situação
@transf_outro_banco_outro_titular_ted_pos_hora
Cenario: Realizar um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "290501" 
E eu clicar no botao "Continuar"
# Inserir a checagem da mensagem de aviso aqui
E eu verificar a "Mensagem de Aviso do Horario"
E eu clicar no botao "Sim"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190625_001-EXECUTA TESTE UTILIZANDO ASSINATURA ELETRONICA INVÁLIDA
# Código Implemtnado por: VALMIR SOARES
# Última Data de Execução de Teste: 25/06/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar o teste de uma operação, 
# utilizando uma assinatura eletrônica inválida. O teste deve verificar acima
# mensagem que é exibida pelo sistema.
@transf_outro_banco_outro_titular_ted_assina_elet_divergente
Cenario: Realizar um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu digitar o valor da transferência "290501" 
E eu clicar no botao "Continuar"
# Inserir a checagem da mensagem de aviso aqui
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Invalida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu verifico a "Mensagem de Assinatura Divergente"

###########################################################################################
###########################################################################################
################################## POUPANCA ###############################################
###########################################################################################
###########################################################################################

# 20190528_001-EXECUTA UMA TRANSFERÊNCIA TED PARA POUPANÇA, PARA OUTRO BANCO
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 28/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar uma TED da Conta Corrente para Conta Poupança
# de outro banco e de Mesma Titularidade
@transf_outro_banco_ted_poupanca
Cenario: Realizar um TED para outro banco 
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "15000" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 201905_012-EXECUTA AGENDAMENTO DE TED PARA OUTRO BANCO, EM CONTA POUPANÇA, MESMA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 16/05/2019
# Neste cenário o sistema deve executar a Transferência via TED, para Conta Poupança
# de Outro Banco e Mesma Titularidade
@transf_outro_banco_ted_poupanca_agendu
Cenario: Realizar o agendamento de uma TED para outro banco em conta poupança 
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "234567" 
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190522_004-EXECUTA UMA TRANSFERÊNCIA DOC PARA CONTA POUPANÇA DE OUTRO BANCO E MESMA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 22/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar um DOC para Conta Poupança de Outro Banco
# e de Mesma Titularidade - Antes do Horário máximo das 17:00HS
@transf_outro_banco_doc_poupanca
Cenario: Realizar um DOC para outro banco
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "220504" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190523_005-EXECUTA O AGENDAMENTO DE UM DOC PARA OUTRO BANCO, EM CONTA POUPANÇA, MESMA TITULARIDADE
# Código Implemtnado por: VALMIR SOARES
# Última Data de Execução de Teste: 23/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar a Transferência via DOC, para Conta Poupança de 
# Outro Banco e Mesma Titularidade
# Transferencia para Dia Não Útil sendo Feriado Nacional - 20/06/2019 - Quinta Feira
@transf_outro_banco_doc_poupanca_agendu
Cenario: Realizar agendamento de um DOC para outro banco
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "230505"
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190522_006-EXECUTA UMA TRANSFERÊNCIA DOC PARA CONTA POUPANÇA DE OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 22/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar um DOC para Conta Poupança de Outro Banco
# e de Outra Titularidade - Antes do Hoário máximo 17:00HS
@transf_outro_banco_outro_titular_doc_poupanca 
Cenario: Realizar um DOC para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta doc"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "200506" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190523_009-EXECUTA AGENDAMENTO DE DOC PARA OUTRO BANCO, EM CONTA POUPANÇA, OUTRA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 23/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar a Transferência via DOC para Conta Poupança 
# de Outro Banco e para Outra Titularidade
# Transferencia para Dia Não Útil sendo Feriado Nacional - 20/06/2019 - Quinta Feira
@transf_outro_banco_outro_titular_doc_poupanca_agendu 
Cenario: Realizar o agendamento de um DOC para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "DOC"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta doc"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "230509" 
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190610_005 EXECUTA UMA TRANSFERÊNCIA TED PARA CONTA POUPANÇA DE OUTRO BANCO E OUTRA TITULARIDADE
# Código Implementado por: VICTOR DEBAZA
# Última Data de Execução de Teste: 10/06/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar uma TED para Conta Poupança de Outro Banco
# e de Outra Titularidade
# Teste executa a transferência em Dia Não Útil, sendo Domingo - 26/05/2019
@transf_outro_banco_outro_titular_ted_poupanca 
Cenario: Realizar um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "100619" 
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso

# 20190523_15-EXECUTA AGENDAMENTO DE TED PARA OUTRO BANCO, EM CONTA POUPANÇA, OUTRA TITULARIDADE
# Código Implementado por: VALMIR SOARES
# Última Data de Execução de Teste: 23/05/2019
# Teste Rodado por: VALMIR SOARES
# Neste cenário o sistema deve executar o Agendamento de uma Transferência via TED
# para Conta Poupança de Outro Banco e Outra Titularidade
# Dias a Somar: 2 ou seja será para dia 26/05/2019 - Dia Náo Útil - Domingo
# Sistema deve agendar para dia: 27/05/2019 - Próximo Dia Útil - Segunda
@transf_outro_banco_outro_titular_ted_poupanca_agendu 
Cenario: Realizar o agendamento de um TED para outro banco e outro titular
Quando eu selecionar a opção "Outros bancos" no menu
E eu clicar em "TED"
E eu clicar no botao "Continuar"
E eu selecionar o banco "341 - BANCO ITAU UNIBANCO S.A."
E eu preencher o campo "Agencia" com "agencia itau"
E eu preencher o campo "Conta" com "conta itau"
E eu selecionar Outro Titular no campo Titularidade
E eu preencher o campo "Nome" com "nome outra conta ted"
E eu preencher o campo "CPF" com "cpf outra conta"
E eu clicar no botao "Continuar"
E eu selecionar a Modalidade "Poupanca"
E eu digitar o valor da transferência "230515" 
E eu clicar no radio button "Agendar transferência para"
E eu clicar no botão Agenda de datas
E eu selecionar uma data d + "Dias a somar"
E eu clicar no botao "Continuar"
E eu verificar os "dados da conta outro banco" a ser tranferida
E eu digitar a assinatura eletronica com "Assinatura Eletronica Valida"
E eu preencher o campo "Código do seu Token" com "Token"
E eu clicar no botao "Continuar"
Então eu recebo a mensagem de sucesso